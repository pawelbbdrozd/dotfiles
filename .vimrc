" download vim-plug if missing
if empty(glob("~/.vim/autoload/plug.vim"))
  silent! execute '!curl --create-dirs -fsSLo ~/.vim/autoload/plug.vim https://raw.github.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * silent! PlugInstall
endif

" declare plugins
silent! if plug#begin()
  
  Plug 'sheerun/vim-polyglot'
  Plug 'mboughaba/i3config.vim'
  Plug 'chrisbra/csv.vim'
  Plug 'ap/vim-css-color'

  call plug#end()
endif

